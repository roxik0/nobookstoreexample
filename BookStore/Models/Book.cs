﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Models
{
    class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public Guid Author { get; set; }

    }
}
