﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Models
{
    class Author
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
    }
}
