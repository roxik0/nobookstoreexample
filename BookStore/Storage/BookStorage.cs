﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BookStore.Models;

namespace BookStore.Storage
{
    static class BookStorage
    {
        public static List<Book> Books = new List<Book>()
        {
            new Book()
            {
                Id = 1,
                Title = "Imię Róży",
                Author = AuthorStorage.Authors.First(p=>p.Name == "Umberto Eco").Guid
            }
        };


    }
}
